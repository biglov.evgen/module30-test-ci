from fastapi.testclient import TestClient

from src.main import app

client = TestClient(app)


fake_db = {
    "recipe": {
        "id": 1,
        "title": "title",
        "cooking_time": 1,
        "view_count": 1,
        "description": "description",
    }
}


def test_recipes():
    response = client.get("/recipes")
    assert response.status_code == 200


def test_recipe_detail():
    response = client.get("/recipe/1")
    assert response.status_code == 200
    assert response.json() == {
        "id": 1,
        "title": "title",
        "cooking_time": 1,
        "view_count": 1,
        "description": "description",
    }
