from pydantic import BaseModel


class Recipe(BaseModel):
    title: str
    cooking_time: int


class IngredientSchema(BaseModel):
    title: str


class RecipeDetail(Recipe):
    description: str
    ingredients: list[IngredientSchema]

    class Config:
        from_attributes = True


class RecipesList(Recipe):
    view_count: int

    class Config:
        from_attributes = True
