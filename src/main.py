from contextlib import asynccontextmanager
from typing import List

from fastapi import FastAPI
from sqlalchemy import desc, select
from sqlalchemy.orm import joinedload

import src.schemas as schemas
from src.database import engine, session
from src.models import Base, Recipe

app = FastAPI()


@app.on_event("startup")
async def shutdown():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
        await conn.commit()


@asynccontextmanager
async def startup():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


@app.get("/")
async def main_page():
    return "OK", 200


@app.get("/recipes", response_model=List[schemas.RecipesList])
async def recipes() -> List[Recipe]:
    res = await session.execute(select(Recipe).order_by(desc("view_count")))
    return res.scalars().all()


@app.get("/recipe/{recipe_id}", response_model=schemas.RecipeDetail)
async def get_recipe(recipe_id: int) -> Recipe:
    stmt = (
        select(Recipe)
        .options(
            joinedload(Recipe.ingredients),
        )
        .where(Recipe.id == recipe_id)
    )
    return await session.scalar(stmt)
