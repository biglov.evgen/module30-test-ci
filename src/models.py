from sqlalchemy import Column, ForeignKey, Integer, String, Table
from sqlalchemy.orm import relationship

from src.database import Base


class Recipe(Base):
    """Модель для описания рецепта в БД"""

    __tablename__ = "recipe"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    cooking_time = Column(Integer, nullable=False, index=True)
    view_count = Column(Integer, default=0, index=True)
    description = Column(String, index=True)
    ingredients = relationship("Ingredient", secondary=lambda: recipe_to_ingredients)


class Ingredient(Base):
    """Модель для описания ингредиента в БД"""

    __tablename__ = "ingredient"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)

    def __repr__(self):
        return self.title


recipe_to_ingredients = Table(
    "recipe_to_ingredients",
    Base.metadata,
    Column("recipe_id", ForeignKey("recipe.id")),
    Column("ingredients_id", ForeignKey("ingredient.id")),
)
